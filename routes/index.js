var RenderPDF = require('chrome-headless-render-pdf');
var { uuid } = require('uuidv4');
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* GET PDF page. */
router.get('/render', function(req, res, next) {
  const url = req.param('url');
  const filename = uuid() + '.pdf';
  RenderPDF.generateSinglePdf(url, filename);
  res.render('render', { filename });
});

router.get('/pdf', function(req, res, next) {
  const filename = req.param('filename');
  res.download(filename);
});

module.exports = router;
